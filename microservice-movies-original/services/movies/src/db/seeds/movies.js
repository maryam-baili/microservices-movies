exports.seed = (knex) => {
  return knex('movies').del()
  .then(() => {
    return Promise.all([
      knex('movies').insert({
        user_id: 1,
        title: 'Jurassic Park',
      })  // eslint-disable-line
    ]);
  })
  .catch((err) => { console.log(err); }); // eslint-disable-line
};
